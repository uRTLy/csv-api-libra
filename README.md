# Libra - API Operations

## Responsibility
## Functions
## Requirements

## Getting Started - Local Development
### Installation
To get started locally, follow these instructions:

1. If you haven't done it already, [make a fork of this repo](http).
1. Clone to your local computer using `git`.
1. Make sure that you have Node 10.16.0 / 11.15.0 or later installed. See instructions [here](https://nodejs.org/en/download/).
1. Make sure that you have `yarn` installed; see instructions [here](https://yarnpkg.com/lang/en/docs/install/).
1. Run `npm config set scripts-prepend-node-path auto`
1. Run `yarn` (no arguments) from the root of your clone of this project.
1. Run `yarn link` to add all custom scripts we use to your global install.
1. Run `yarn run build` from the root of your clone of this project.
1. Copy file `env.skeleton` to `.env` and fill env's variables e.g. PORT=3000
1. Run `yarn run serve` and visit link `http://localhost:3000`.
