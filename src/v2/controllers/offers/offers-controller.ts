import { Response, Request, Router } from 'express';
import { Offer } from './offers-interface';
import { IAnswerAPI, APIRouter } from '../../../common/interfaces/api.interface';
import { FakeDb } from '../../../common/lib/fake-db';

export class OffersController extends APIRouter {
  public path = '/actualoffers';
  public router = Router();
  private offers: Array<Offer>;
  private db: FakeDb;

  constructor(db: FakeDb) {
    super();
    this.db = db;
    this.initializeRoutes();
  }

  public initializeRoutes(): void {
    this.router.get(this.path, this.getOffers);
    this.router.get(this.path + '/:id', this.getOffer);
    this.router.post(this.path, this.createOffer);
  }
  /**
   * Get all offers
   */
  public getOffers = (request: Request, response: Response): Response => {
    const answer: IAnswerAPI = {
      object: 'lists',
      url: request.url,
      has_more: false,
      page: 1,
      per_page: 5,
      total: 1,
      total_pages: 1,
      data: this.offers,
    };
    return response.json(answer);
  };

  /**
   * Get one offer by id
   */
  public getOffer = (request: Request, response: Response): Response => {
    // FIXME w(psasin): get one record
    return response.json(request.params);
  };

  /**
   * Create one offer
   */
  public createOffer = (request: Request, response: Response): Response => {
    const offer: Offer = request.body;
    return response.send(offer);
  };
}
