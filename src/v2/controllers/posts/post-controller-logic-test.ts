jest.setTimeout(30000);

import { PostsController } from './posts-controller';

const controller = new PostsController();

describe('PostsController', () => {
  it('should return instance when new operator used', (done) => {
    expect(controller).toBeInstanceOf(PostsController);
    done();
  });

  it('should have constructor', (done) => {
    expect(PostsController.prototype.constructor).toBeTruthy();
    done();
  });

  it('should have method of --router--', (done) => {
    expect(typeof controller.router).toBe('function');
    done();
  });

  it('should have method of --initializeRoutes--', (done) => {
    expect(controller.initializeRoutes).toBeTruthy();
    done();
  });

  it('should have path = /posts', (done) => {
    expect(controller.path).toBe('/posts');
    done();
  });
});
