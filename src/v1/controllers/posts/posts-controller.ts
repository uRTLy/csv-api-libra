import { Response, Request, Router } from 'express';
import { Post } from './post-interface';
import { IAnswerAPI, APIRouter } from '../../../common/interfaces/api.interface';

export class PostsController extends APIRouter {
  public path = '/posts';
  public router = Router();

  private posts: Array<Post> = [
    {
      author: 'Pawel Sasin',
      content: 'Dolor sit amet',
      title: 'Lorem Ipsum',
    },
  ];

  constructor() {
    super();
    this.initializeRoutes();
  }

  public initializeRoutes(): void {
    this.router.get(this.path, this.getPosts);
    this.router.get(this.path + '/:id', this.getPost);
    this.router.post(this.path, this.createPost);
  }
  /**
   * Get all posts
   */
  public getPosts = (request: Request, response: Response): Response => {
    const answer: IAnswerAPI = {
      object: 'lists',
      url: request.url,
      has_more: false,
      page: 1,
      per_page: 5,
      total: 1,
      total_pages: 1,
      data: this.posts,
    };
    return response.json(answer);
  };

  /**
   * Get one post by id
   */
  public getPost = (request: Request, response: Response): Response => {
    // FIXME (psasin): get one record
    return response.json(request.params);
  };

  /**
   * Create one post
   */
  public createPost = (request: Request, response: Response): Response => {
    const post: Post = request.body;
    this.posts.push(post);
    return response.send(post);
  };
}
