import dotenv from 'dotenv';
const validconfig = dotenv.config({ path: '.env' });

if (validconfig.error) {
  throw 'The environment of file (.env) not found...';
}

import { StartController } from './common/controllers/start/start-controller';
import { HeartbeatController } from './common/controllers/heartbeat/heartbeat-controller';
import { AuthController } from './common/controllers/auth/auth-controller';
import { PostsController } from './v1/controllers/posts/posts-controller';

import { App } from './app';

// common points e.g. /docs,  /auth, /heartbeat
const zero = [
  // before authorization
  new HeartbeatController(),
  new StartController(),
];

// Points in /api/v1/
const v1 = [
  // before authorization
  new AuthController(),
  // after authorization
  new PostsController(),
];

// Points in /api/v2/ in future, sample
const v2 = [
  // before authorization
  new AuthController(),
  // after authorization
  new PostsController(),
];

const app = new App(zero, v1, v2, parseInt(process.env.PORT, 10));
app.listen();
