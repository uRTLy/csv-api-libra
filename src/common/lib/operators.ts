import isEqual from 'lodash.isequal';
import { OPERATORS } from '../consts/constants';

export const hasOperators = (obj: any): boolean =>
  Object.keys(obj).some((prop) => Object.keys(OPERATORS).includes(prop));

export const isOperator = (prop: any) => !!OPERATORS[prop];

export const search = (value: any, recordValue: any): boolean =>
  value instanceof RegExp
    ? value.test(recordValue)
    : recordValue.includes(value) || value.includes(recordValue);

export const getOperators = (recordValue: any) => {
  return {
    ne: (value: any) => !isEqual(value, recordValue),
    gte: (value: any) => value >= recordValue,
    lte: (value: any) => value <= recordValue,
    like: (value: any) => !isEqual(value, recordValue),
    q: (value: any) => search(value, recordValue),
  };
};

export const handleOperators = (searched: any, record: any) => {
  const operators = Object.keys(searched).filter(isOperator);
  const operatorsConditions = getOperators(record);

  return operators.every((operator) => {
    const operatorFunction = operatorsConditions[operator];
    const operatorValue = searched[operator];

    return operatorFunction(operatorValue);
  });
};
