import { QueryParams, DBRecords, DBRecord, Relation } from '../consts/common-types';
import { checkConditions, checkEquality } from './query-helpers';
import { FakeDb } from './fake-db';

export class Query {
  private query: QueryParams;
  private data: DBRecords;
  private methods: any;

  constructor(
    data: DBRecords,
    query: QueryParams,
    methods?: { getRelations: FakeDb['getRelations'] },
  ) {
    this.data = data;
    this.query = query;
    this.methods = methods || {};
  }

  execute(): DBRecords {
    return [this.embed, this.find, this.sort, this.slice, this.paginate].reduce(
      (filtered, method) => method(filtered),
      this.data || [],
    );
  }

  find = (filtered: DBRecords): DBRecords => {
    if (!this.query.prop) {
      return filtered;
    }
    const id = this.query.prop.id;
    const queryKeys = Object.keys(this.query.prop);

    if (id && queryKeys.length === 1) {
      return filtered.filter((item) => checkEquality(id, item.id));
    } else {
      return filtered.filter((record) => checkConditions(record, this.query));
    }
  };

  sort = (filtered: DBRecords): DBRecords => {
    const { sort, order = ['asc'] } = this.query;
    if (!sort) {
      return filtered;
    }
    return sort.reduce((data, sortProp, index) => {
      const currentOrder = order[index] || 'asc';

      const orders = {
        asc: (a, b) => a[sortProp] - b[sortProp],
        desc: (a, b) => b[sortProp] - a[sortProp],
      };

      return data.slice(0).sort(orders[currentOrder]);
    }, filtered);
  };

  slice = (filtered: DBRecords) => {
    const { start, end } = this.query;
    if (start && end) {
      return filtered.slice(start, end);
    }

    return filtered;
  };

  embed = (filtered: DBRecords) => {
    if (!this.query.embed || !this.methods.getRelations) {
      return filtered;
    }

    return filtered.map((record) => this.methods.getRelations(record, this.query.embed));
  };

  paginate = (filtered: DBRecords) => {
    const { page, limit = 10 } = this.query;

    if (!page) {
      return filtered;
    }
    const pages = [];
    const maxPages = filtered.length / limit;

    for (let i = 0; i < maxPages; i++) {
      pages.push(filtered.slice(i * limit, (i + 1) * limit));
    }

    return pages[page];
  };
}
