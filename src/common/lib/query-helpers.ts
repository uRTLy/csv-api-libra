import isEqual from 'lodash.isequal';

import { QueryParams, DBRecord } from '../consts/common-types';
import { hasOperators, handleOperators } from './operators';

const NESTED_KEY_DOT = '.';

export const getNestedProperties = (record: DBRecord, queryProp: QueryParams['prop']) => {
  const nestedKeys = Object.keys(queryProp).filter((key) => key.includes(NESTED_KEY_DOT));
  if (nestedKeys.length === 0) {
    return record;
  }
  return nestedKeys.reduce((props, nKey) => {
    const splitted = nKey.split(NESTED_KEY_DOT);
    const value = splitted.reduce((r: DBRecord, sKey) => r[sKey], record);
    return { ...props, [nKey]: value };
  }, record);
};

export const checkEquality = (searched: any, record: any): boolean => {
  if (Array.isArray(searched)) {
    return searched.some((val) => isEqual(val, record));
  }

  if (typeof searched !== 'object' || !hasOperators(searched)) {
    return isEqual(searched, record);
  }
  return handleOperators(searched, record);
};

export const orCond = (record: DBRecord, conditions: Partial<DBRecord> = {}): boolean => {
  const recordNestedProps = getNestedProperties(record, conditions);

  return Object.keys(conditions).some((key: keyof DBRecord) =>
    checkEquality(conditions[key], recordNestedProps[key]),
  );
};

export const andCond = (record: DBRecord, conditions: Partial<DBRecord> = {}): boolean => {
  const recordNestedProps = getNestedProperties(record, conditions);

  return Object.keys(conditions).every((key: keyof DBRecord) =>
    checkEquality(conditions[key], recordNestedProps[key]),
  );
};

export const checkConditions = (record: DBRecord, query: QueryParams): boolean => {
  const { and, or, ...restProps } = query.prop;

  if (!and && !or) {
    return restProps ? orCond(record, restProps) : false;
  }
  const orResult = orCond(record, or);
  const andResult = andCond(record, and);
  if (and && or) {
    return andResult || orResult;
  }

  if (or) {
    return orResult;
  }

  if (and) {
    return andResult;
  }
};
