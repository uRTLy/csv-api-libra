import { createReadStream, readdir } from 'fs';
import { promisify } from 'util';
import { resolve } from 'path';

import csv from 'csv-parse';

import { FileObject } from '../consts/common-types';
import { CSVFiles } from '../consts/constants';

const fsReadDir = promisify(readdir);

export const dashCaseToKebabCase = (field: string): string =>
  field
    .split('_')
    .map((part: string, index) =>
      index === 0 ? part : `${part.charAt(0).toUpperCase()}${part.slice(1)}`,
    )
    .join('');

export const filterCSV = (file: string) => {
  const split = file.split('.');
  const extension = split[split.length - 1];
  return extension === 'csv';
};

export const parseCSVFile = async (file: string): Promise<Array<FileObject>> => {
  return await new Promise((resolve, reject) => {
    const allData: Array<any> = [];
    createReadStream(file)
      .pipe(csv({ columns: (headers) => headers.map(dashCaseToKebabCase) }))
      .on('data', (data: any) => allData.push(data))
      .on('end', () => resolve(allData))
      .on('error', reject);
  });
};

export const parseData = async (dir: string): Promise<FileObject> => {
  try {
    const path = resolve(__dirname, dir);
    const files: Array<string> = await fsReadDir(path);
    const csvFiles = files.filter(filterCSV);

    const fileObjects: Array<FileObject> = [];

    for (let file of csvFiles) {
      const parsed = await parseCSVFile(resolve(path, file));
      let fileName = (CSVFiles[file] as string) || file.split('.')[0];

      fileObjects.push({ [fileName]: parsed });
    }

    return fileObjects.reduce((obj, file) => ({ ...obj, ...file }), {});
  } catch (e) {
    console.error(e);
  }
};
