import {
  DBRecord,
  DBRecords,
  DBTable,
  FileObject,
  Relation,
  QueryParams,
} from '../consts/common-types';

import { parseData } from './prepare-csv';
import { Query } from './query';

export class FakeDb {
  private dir: string;
  private tables: DBTable;
  private mapped: DBRecords;
  private relations: Array<Relation>;

  constructor(dir: string) {
    this.dir = dir;
    this.tables = {};
    this.relations = [];
  }

  async connect() {
    this.tables = await parseData(this.dir);
  }

  addRelations(relations: Array<Relation>): void {
    this.relations = relations;
  }

  mapRelations(primaryTable: keyof DBTable | DBRecords, relations?: Array<Relation>): DBRecords {
    const records = Array.isArray(primaryTable) ? primaryTable : this.tables[primaryTable];

    const rels = relations || this.relations;
    const mapped = records.map((record) => {
      const related = this.getRelations(record, rels.map(({ relationName }) => relationName));

      return {
        ...record,
        ...related,
      };
    });
    return mapped;
  }

  getRelations(record: DBRecord, embededRelations: Array<Relation['relationName']>) {
    const relations = this.relations.filter((relation) =>
      embededRelations.includes(relation.relationName),
    );

    return relations.reduce((all, relation) => {
      const { enter, tableName, relationName, through } = relation;

      let relatedItems: DBRecords;

      if (through) {
        const relationMaps = this.find(through.table, {
          [through.enter.related]: record[through.enter.primary],
        });
        const maps = relationMaps.map((map) => map[enter.related]);
        relatedItems = this.find(tableName, { [enter.primary]: maps });
      } else {
        relatedItems = this.find(tableName, { [enter.related]: record[enter.primary] });
      }

      return {
        ...all,
        [relationName]: relatedItems,
      };
    }, {});
  }

  table(tableName: keyof DBTable) {
    return {
      query: (q: QueryParams) =>
        new Query(this.tables[tableName], q, { getRelations: this.getRelations }),
      getRecords: () => this.tables[tableName],
    };
  }

  find(tableName: keyof DBTable, prop: { [key: string]: any }) {
    return this.table(tableName)
      .query({ prop })
      .execute();
  }

  getTable(tableName: keyof DBTable) {
    const data = this.tables[tableName];

    return {
      query: (query: QueryParams) => new Query(data, query, { getRelations: this.getRelations }),
    };
  }

  setTable(tableName: string | number, tableData: DBRecords): void {
    this.tables[tableName] = tableData;
  }

  getRecords(): DBRecords {
    return this.mapped;
  }
}
