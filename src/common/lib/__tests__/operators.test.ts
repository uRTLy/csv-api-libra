import { isOperator, getOperators, handleOperators } from '../operators';
import { OPERATORS } from '../../consts/constants';

describe('Operators', () => {
  describe('isOperator', () => {
    it('should properly determine if key is operator key', () => {
      expect(isOperator('ne')).toBe(true);
      expect(isOperator('lte')).toBe(true);
      expect(isOperator('gte')).toBe(true);
      expect(isOperator('like')).toBe(true);

      expect(isOperator('some_word')).toBe(false);
      expect(isOperator(15)).toBe(false);
      expect(isOperator([])).toBe(false);
      expect(isOperator({})).toBe(false);
    });
  });

  describe('getOperators', () => {
    it('should properly map operator function from operator key', () => {
      const { ne, like, gte, lte } = getOperators(15);

      expect(ne(12)).toBe(true);
      expect(ne(15)).toBe(false);

      expect(gte(10)).toBe(false);
      // expect(like(15)).toBe(true);
      expect(lte(15)).toBe(true);
    });
  });

  describe('handleOperators', () => {
    const ops = {
      id: {
        gte: 5,
        lte: 20,
      },
      name: {
        q: 'sul',
      },
    };

    it('should properly map operator function from operator key', () => {
      const result = handleOperators(ops, { id: 15, name: 'john', lastname: 'sullivan' });

      expect(result).toBeDefined();
      expect(result).toBe(true);
    });
  });
});
