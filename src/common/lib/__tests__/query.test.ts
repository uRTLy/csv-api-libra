import { FakeDb } from '../fake-db';
import { Query } from '../query';

import { DATA_DIR } from '../../consts/constants';

describe('Query', () => {
  let db;
  let books;

  beforeAll(async () => {
    db = new FakeDb(DATA_DIR);
    await db.connect();
    books = db.table('books').getRecords();
  });

  it('should be defined', () => {
    expect(Query).toBeDefined();
  });

  describe('find', () => {
    it('by nested properties', () => {
      const nestedPropsData = [
        { author: { name: 'joe', lastName: 'sullivan' } },
        { author: { name: 'tom', lastName: 'sullivan' } },
        { author: { name: 'ronald', lastName: 'sullivan' } },
        { author: { name: 'william', lastName: 'sullivan' } },
      ];

      const queryResult = new Query(nestedPropsData, {
        prop: { 'author.name': 'joe' },
      }).execute();

      expect(queryResult).toBeDefined();
      expect(queryResult).toHaveLength(1);
      expect(queryResult[0].author.name).toBe('joe');
    });

    it('by id', () => {
      const queryResult = new Query(books, { prop: { id: '207611' } }).execute();

      expect(queryResult).toBeDefined();
      expect(queryResult).toHaveLength(1);
      expect(queryResult[0].id).toBe('207611');
    });

    it('by an array of ids', () => {
      const queryResult = new Query(books, {
        prop: { id: ['207611', '207613', '207610'] },
      }).execute();

      expect(queryResult).toBeDefined();
      expect(queryResult).toHaveLength(3);
    });

    it('by multiple props', () => {
      const queryResult = new Query(books, {
        prop: { id: '207629', isbn: '978-83-08-05510-6' },
      }).execute();

      expect(queryResult).toBeDefined();
      expect(queryResult).toHaveLength(1);
      expect(queryResult[0].id).toBe('207629');
      expect(queryResult[0].isbn).toBe('978-83-08-05510-6');
    });

    it('by multiple props with arrays of values', () => {
      const queryResult = new Query(books, {
        prop: { id: ['207629', '207613'], isbn: ['978-83-08-05510-6', '978-83-66134-99-7'] },
      }).execute();

      expect(queryResult).toBeDefined();
      expect(queryResult).toHaveLength(3);
      expect(queryResult[0].id).toBe('207629');
      expect(queryResult[0].isbn).toBe('978-83-08-05510-6');
      expect(queryResult[1].id).toBe('207613');
      expect(queryResult[2].isbn).toBe('978-83-66134-99-7');
    });

    it('with OR clause', () => {
      const queryResult = new Query(books, {
        prop: { or: { id: '207629', isbn: '978-83-08-05510-6' } },
      }).execute();

      expect(queryResult).toBeDefined();
      expect(queryResult).toHaveLength(1);
      expect(queryResult[0].id).toBe('207629');
      expect(queryResult[0].isbn).toBe('978-83-08-05510-6');
    });

    it('with AND clause', () => {
      const queryResult = new Query(books, {
        prop: { id: '207629', isbn: '978-83-08-05510-6' },
      }).execute();

      expect(queryResult).toBeDefined();
      expect(queryResult).toHaveLength(1);
      expect(queryResult[0].id).toBe('207629');
      expect(queryResult[0].isbn).toBe('978-83-08-05510-6');
    });

    describe('operators', () => {
      it('ne', () => {
        const queryResult = new Query(books, { prop: { id: { ne: '207629' } } }).execute();
        // expect(queryResult.length).toBe(5);
      });

      it('lte', () => {
        // const queryResult = new Query(books, { start: 5, end: 10 }).execute();
        // expect(queryResult.length).toBe(5);
      });

      it('gte', () => {
        // const queryResult = new Query(books, { start: 5, end: 10 }).execute();
        // expect(queryResult.length).toBe(5);
      });

      it('like', () => {
        // const queryResult = new Query(books, { start: 5, end: 10 }).execute();
        // expect(queryResult.length).toBe(5);
      });
    });
  });

  describe('slice', () => {
    it('should work for basic cases', () => {
      const queryResult = new Query(books, { start: 5, end: 10 }).execute();

      expect(queryResult.length).toBe(5);
    });
  });

  describe('paginate', () => {
    it('should work for basic cases', () => {
      const queryResult = new Query(books, { page: 2, limit: 2 }).execute();
      expect(queryResult.length).toBe(2);
    });
  });

  describe('sort and order', () => {
    const wrongOrder = [
      { id: 3, name: 'job' },
      { id: 2, name: 'job' },
      { id: 4, name: 'job' },
      { id: 1, name: 'job' },
    ];

    it('should work for basic cases', () => {
      const queryResult = new Query(wrongOrder, { sort: ['id'], order: ['asc'] }).execute();

      expect(queryResult).toBeDefined();
      expect(queryResult[0].id).toBe(1);
    });
    it('should work for descending sorting', () => {
      const queryResult = new Query(wrongOrder, { sort: ['id'], order: ['desc'] }).execute();

      expect(queryResult).toBeDefined();
      expect(queryResult[0].id).toBe(4);
    });
  });
});
