import { FakeDb } from '../fake-db';
import { DATA_DIR } from '../../consts/constants';

describe('Fake DB', () => {
  it('should exist', () => {
    expect(FakeDb).toBeDefined();
  });

  it('should properly map basic relations', async () => {
    expect(FakeDb).toBeDefined();
    const table = {
      articles: [
        { id: 100, content: 'bla bla' },
        { id: 101, content: 'bla bla' },
        { id: 102, content: 'bla bla' },
        { id: 103, content: 'bla bla' },
      ],
      posts: [
        { id: 1, articleId: 100, content: 'bla post' },
        { id: 2, articleId: 100, content: 'bla post' },
        { id: 3, articleId: 100, content: 'bla post' },
        { id: 4, articleId: 100, content: 'bla post' },
      ],
    };

    const basicRelations = [
      { tableName: 'posts', enter: { primary: 'id', related: 'articleId' }, relationName: 'posts' },
    ];
    const db = new FakeDb(DATA_DIR);
    await db.connect();

    const mappedRelations = db.mapRelations(table.articles, basicRelations);
    expect(mappedRelations).toBeDefined();
  });

  it('should properly map through relations', async () => {
    const table = {
      books: [
        { id: 100, content: 'bla bla' },
        { id: 101, content: 'bla bla' },
        { id: 102, content: 'bla bla' },
        { id: 103, content: 'bla bla' },
      ],
      tags: [
        { id: 1, name: 'tag1' },
        { id: 2, name: 'tag2' },
        { id: 3, name: 'tag3' },
        { id: 4, name: 'tag4' },
      ],
      tagsRelations: [
        { id: 1, bookId: 100, tagId: 1, content: 'bla post' },
        { id: 2, bookId: 100, tagId: 2, content: 'bla post' },
        { id: 3, bookId: 100, tagId: 3, content: 'bla post' },
        { id: 4, bookId: 100, tagId: 4, content: 'bla post' },
      ],
    };
    const relations = [
      {
        tableName: 'tags',
        through: { table: 'tagsRelations', enter: { primary: 'id', related: 'bookId' } },
        enter: { primary: 'id', related: 'tagId' },
        relationName: 'tags',
      },
    ];
    const db = new FakeDb('./');

    db.setTable('tags', table.tags);
    db.setTable('tagsRelations', table.tagsRelations);
    db.addRelations(relations);

    const mappedRelations = db.mapRelations(table.books, relations);

    expect(mappedRelations).toBeDefined();
    expect(mappedRelations[0].tags).toBeDefined();
    expect(mappedRelations[0].tags).toHaveLength(4);
  });

  it('should work with proper test csv data', async () => {
    expect(FakeDb).toBeDefined();

    const db = new FakeDb(DATA_DIR);
    await db.connect();

    const relations = [
      {
        tableName: 'allPublishers',
        enter: { primary: 'wydawnictwoId', related: 'id' },
        relationName: 'wydawnictwo',
      },

      {
        tableName: 'catLevels',
        enter: { primary: 'kategoriaId', related: 'katid' },
        relationName: 'kategoria',
      },
      {
        tableName: 'tagsDict',
        through: { table: 'tagsRelations', enter: { primary: 'id', related: 'ksiazkaId' } },
        enter: { primary: 'id', related: 'zagadnienieId' },
        relationName: 'zagadnienie',
      },
    ];

    db.addRelations(relations);
    const mappedRelations = db.mapRelations('books', relations);

    expect(mappedRelations.some((rel) => rel.kategoria.length > 0)).toBe(true);
    expect(mappedRelations.some((rel) => rel.wydawnictwo.length > 0)).toBe(true);

    // none of relations from tags_relations.csv is related to books from 20books.csv (?)
    // expect(mappedRelations.some((rel) => rel.zagadnienie.length > 0)).toBe(true);
  });
});
