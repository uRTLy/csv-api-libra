import mock from 'mock-fs';
import { parseData, dashCaseToKebabCase, filterCSV } from '../prepare-csv';

describe('helpers', () => {
  describe('dashCaseToKebabCase', () => {
    it('should properly format strings', () => {
      expect(dashCaseToKebabCase('author_name')).toEqual('authorName');
      expect(dashCaseToKebabCase('author_name_')).toEqual('authorName');
      expect(dashCaseToKebabCase('author_mother_name')).toEqual('authorMotherName');
    });
  });

  describe('filterCSV', () => {
    it('should return true for .csv extensions', () => {
      expect(filterCSV('file.csv')).toEqual(true);
      expect(filterCSV('other.file.csv')).toEqual(true);
      expect(filterCSV('csv.file.txt')).toEqual(false);
    });
  });
});

describe('CSV Parser', () => {
  beforeEach(() => {
    // setting jest test timeout to 30 secs because defualt 5 secs is not always enough
    jest.setTimeout(30000);
  });

  describe('basic error handling', () => {
    let emptyDir = '/empty-dir';

    it('function parseData should exist', async () => {
      expect(parseData).toBeDefined();
    });

    beforeEach(() => {
      mock({
        [emptyDir]: {},
      });
    });

    it('should return empty object when directory is empty', async () => {
      const result = await parseData(emptyDir);
      expect(result).toEqual({});
    });

    afterEach(() => {
      mock.restore();
    });
  });

  describe('given directory with .csv files', () => {
    const dir = '/data';

    beforeEach(() => {
      mock({
        '/data': {
          'some-file.csv': `header1,header2,header3,header4,header5\nval1,val2,val3,val4,val5`,
          'not-csv-file.txt': 'contents',
        },
      });
    });

    it('should ignore other files than .csv ', async () => {
      const result = await parseData(dir);
      expect(Object.keys(result)).toHaveLength(1);
    });

    it('should read them all and parse to object with file name as a key ', async () => {
      const result = await parseData(dir);

      expect(result).toEqual({
        'some-file': [
          {
            header1: 'val1',
            header2: 'val2',
            header3: 'val3',
            header4: 'val4',
            header5: 'val5',
          },
        ],
      });
    });

    afterEach(() => {
      mock.restore();
    });
  });
});
