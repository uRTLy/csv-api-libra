import {
  checkEquality,
  checkConditions,
  andCond,
  orCond,
  getNestedProperties,
} from '../query-helpers';

const records = [
  { name: 'Tom', prop1: 'someval1', id: 149 },
  { name: 'Joe', prop1: 'someval2', id: 148 },
  { name: 'Adam', prop1: 'someval3', id: 147 },
  { name: 'John', prop1: 'someval', id: 146 },
  { name: 'Brad', prop1: 'someval', id: 145 },
  { name: 'Pepe', prop1: 'someval6', id: 144 },
  { name: 'Hans', prop1: 'someval', id: 143 },
];

describe('Query helpers', () => {
  describe('getNestedProperties', () => {
    const nestedPropsData = [
      { author: { name: 'joe', lastName: 'sullivan' } },
      { author: { name: 'tom', lastName: 'sullivan' } },
      { author: { name: 'ronald', lastName: 'sullivan' } },
      { author: { name: 'william', lastName: 'sullivan' } },
    ];

    it('should work for shallow nest', () => {
      const result = getNestedProperties(nestedPropsData[0], {
        'author.name': 'joe',
        'author.lastName': 'sullivan',
      });
      expect(result['author.name']).toBe('joe');
      expect(result['author.lastName']).toBe('sullivan');
    });

    it('should work for deeper nest', () => {
      const deeperNestedPropsData = [
        { author: { names: { firstName: 'joe', lastName: 'sullivan' } } },
        { author: { names: { firstName: 'tom', lastName: 'sullivan' } } },
        { author: { names: { firstName: 'ronald', lastName: 'sullivan' } } },
        { author: { names: { firstName: 'william', lastName: 'sullivan' } } },
      ];

      const result = getNestedProperties(deeperNestedPropsData[0], {
        'author.names.firstName': 'joe',
        'author.names.lastName': 'sullivan',
      });

      expect(result['author.names.firstName']).toBe('joe');
      expect(result['author.names.lastName']).toBe('sullivan');
    });
  });

  describe('checkEquality', () => {
    it('should work for basic values', () => {
      const result = checkEquality(149, records[0].id);
      expect(result).toBe(true);
    });
  });

  describe('andCond', () => {
    it('should work for basic one prop search ', () => {
      const andQuery = { and: { id: 149 } };

      expect(andCond(records[0], andQuery.and)).toEqual(true);
    });

    it('should work for multiple props', () => {
      const andQuery = { and: { id: 149, name: 'Tom' } };

      expect(andCond(records[0], andQuery.and)).toEqual(true);
    });
  });

  describe('orCond', () => {
    it('should work for for basic type', () => {
      const orQuery = { or: { id: 149 } };

      expect(orCond(records[0], orQuery.or)).toEqual(true);
    });
    it('should work for multiple props', () => {
      const orQuery = { or: { id: 149, name: 'Tom' } };

      expect(orCond(records[0], orQuery.or)).toEqual(true);
    });
  });

  describe('checkConditions', () => {
    it('should work for basic case', () => {
      const query = { prop: { id: 149 } };
      const result = checkConditions(records[0], query);

      expect(result).toBe(true);
    });

    it('should work for AND clause', () => {
      const query = { prop: { and: { id: 149, name: 'Tom' } } };
      const result = checkConditions(records[0], query);

      expect(result).toBe(true);
    });

    it('should work for OR  clause', () => {
      const query = { prop: { or: { id: 149, name: 'Pepe' } } };
      const result = checkConditions(records[0], query);

      expect(result).toBe(true);
    });

    it('should work for BOTH clauses', () => {
      const query = { prop: { and: { id: 149 }, or: { id: 147, name: 'John' } } };

      const result = checkConditions(records[0], query);

      expect(result).toBe(true);
    });
  });
});
