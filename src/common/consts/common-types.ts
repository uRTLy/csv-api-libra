export type valueof<T> = T[keyof T];

export type DBRecord = { [key: string]: any };

export type DBTable = FileObject;
export type DBRecords = Array<DBRecord>;

export interface Enter {
  primary: any;
  related: any;
}
export interface Relation {
  tableName: keyof DBTable;
  enter: Enter;
  relationName: string;
  through?: { table: keyof DBTable; enter: Enter };
}

export interface FileObject {
  [key: string]: Array<DBRecord>;
}

export interface Operator {
  gte?: Array<any>;
  lte?: Array<any>;
  ne?: Array<any>;
  like?: Array<any>;
}

export interface Condition {
  [key: string]: any | Operator;
  and?: {
    [key: string]: any;
  };
  or?: {
    [key: string]: any;
  };
}

type Order = 'asc' | 'desc';

export interface QueryParams {
  prop?: Condition & Operator;
  id?: number;
  start?: number;
  end?: number;
  page?: number;
  limit?: number;
  sort?: Array<string>;
  order?: Array<Order>;
  q?: string | RegExp;
  embed?: Array<string>;
  expand?: string;
}
