export const DATA_DIR = '../../../data';

export enum CSVFileNames {
  BOOKS = '20books.csv',
  ALL_PUBLISHERS = 'all_publishers.csv',
  CATEGORY_LEVELS = 'cat_levels.csv',
  TAGS_DICT = 'tags_dict.csv',
  TAGS_RELATIONS = 'tags_relations.csv',
}

export enum MappedFiles {
  BOOKS = 'books',
  ALL_PUBLISHERS = 'allPublishers',
  CATEGORY_LEVELS = 'catLevels',
  TAGS_DICT = 'tagsDict',
  TAGS_RELATIONS = 'tagsRelations',
}

export const CSVFiles = {
  [CSVFileNames.BOOKS]: MappedFiles.BOOKS,
  [CSVFileNames.ALL_PUBLISHERS]: MappedFiles.ALL_PUBLISHERS,
  [CSVFileNames.CATEGORY_LEVELS]: MappedFiles.CATEGORY_LEVELS,
  [CSVFileNames.TAGS_DICT]: MappedFiles.TAGS_DICT,
  [CSVFileNames.TAGS_RELATIONS]: MappedFiles.TAGS_RELATIONS,
};

export const OPERATORS = {
  ne: 'ne',
  like: 'like',
  gte: 'gte',
  lte: 'lte',
};
