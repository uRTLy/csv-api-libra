export interface IAnswerAPI {
  object: string;
  url: string;
  has_more: boolean;
  page: number;
  per_page: number;
  total: number;
  total_pages: number;
  data: Array<any | null>;
}

export class APIRouter {
    constructor() {
        if (this.constructor === APIRouter) {
            throw new TypeError('Abstract class "APIRouter" cannot be instantiated directly.'); 
        }

        if (this.initializeRoutes === undefined) {
            throw new TypeError('Classes extending the APIRouter abstract class');
        }
    }
  
    public initializeRoutes(): void {
    }
    
}
